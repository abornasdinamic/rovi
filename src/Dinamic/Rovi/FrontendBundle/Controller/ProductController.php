<?php

namespace Dinamic\Rovi\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Dinamic\Rovi\ProductsBundle\Entity\Product;

/**
 * @Route("/producto/{product}", requirements={"product": "\d+"})
 */
class ProductController extends Controller
{
    /**
     * @Route("/")
     * @Template()
     */
    public function viewAction(Product $product)
    {
        return array('product' => $product);
    }
    
    /**
     * @Route("/descripcion")
     * @Template()
     */
    public function descripcionAction(Product $product)
    {
        return array('product' => $product);
    }

    /**
     * @Route("/herramientas")
     * @Template()
     */
    public function herramientasAction(Product $product)
    {
        return array('product' => $product);
    }
}
