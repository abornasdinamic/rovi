<?php

namespace Dinamic\Rovi\FrontendBundle\Controller;

use Dinamic\Rovi\ProductsBundle\Entity\Product;
use Dinamic\Rovi\ProductsBundle\Entity\ProductBanner;
use Dinamic\Rovi\ProductsBundle\Service\ProductHandler;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @Route("/admin")
 */
class AdminController extends Controller
{
    /**
     * @var ProductHandler
     */
    protected $productHandler;

    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);

        $this->productHandler = $container->get('dinamic.rovi.product_handler');
    }

    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }

    /**
     * @Route("/insertar")
     * @Template()
     */
    public function insertAction()
    {
        // aquí hay que hacer su formulario, etc. etc.

        $productBanner = new ProductBanner();
        $productBanner->setImgBanner('IMAGEBANNER.jpg');
        $productBanner->setUrlBanner('urlBanner');

        $product = new Product();

        $product->setImgLogo('IMAGENLOGO.jpg');
        $product->setImgIcono('IMAGENICONO.jpg');
        $product->setImgFicha('IMAGENFICHA.jpg');
        $product->setDescripcion('Esto es una pedazo de descripción.');
        $product->setProspecto('RUTA/AL/PROSPECTO.pdf');
        $product->addProductBanner($productBanner);

        $this->productHandler->persist($product);
    }

    /**
     * @Route("/editar/{producto}", requirements={"product": "\d+"})
     * @Template()
     */
    public function editAction(Product $product)
    {
        return array('product' => $product);
    }

    /**
     * @Route("/eliminar/{product}", requirements={"product": "\d+"})
     * @Template()
     */
    public function deleteAction(Product $product)
    {
    }
}
