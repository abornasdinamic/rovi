<?php

/*
 * Copyright (c) 2014 Certadia, SL
 * All rights reserved
 */

namespace Dinamic\Rovi\ProductsBundle\Service;

use Dinamic\Rovi\ProductsBundle\Entity\Product;
use Doctrine\ORM\EntityManager;

class ProductHandler
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function persist(Product $product)
    {
        $this->entityManager->persist($product);
        $this->entityManager->flush($product);
    }

    public function delete(Product $product)
    {
        $this->entityManager->remove($product);
        $this->entityManager->flush();
    }
} 