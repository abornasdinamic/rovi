<?php

namespace Dinamic\Rovi\ProductsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Proxies\__CG__\Dinamic\ProductsBundle\Entity\Products;


/**
 * Products
 *
 * @ORM\Table(name="rovi_products")
 * @ORM\Entity(repositoryClass="Dinamic\Rovi\ProductsBundle\Repository\ProductRepository")
 */
class Product
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\OneToMany(targetEntity="ProductBanner", mappedBy="products", cascade={"persist"})
     */
    private $productBanner;

    /**
     * @var string
     *
     * @ORM\Column(name="imgLogo", type="string", length=75)
     */
    private $imgLogo;

    /**
     * @var string
     *
     * @ORM\Column(name="imgIcono", type="string", length=75)
     */
    private $imgIcono;

    /**
     * @var string
     *
     * @ORM\Column(name="imgFicha", type="string", length=75)
     */
    private $imgFicha;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=3500)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="prospecto", type="string", length=3500)
     */
    private $prospecto;

    public function __construct()
    {
        $this->productBanner = new ArrayCollection();
    }

    /**
     * Haz de esto una propiedad de la entidad
     */
    public function getCode()
    {
        return '';
    }

    /**
     * Haz de esto una propiedad de la entidad
     */
    public function getName()
    {
        return '';
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set imgLogo
     *
     * @param string $imgLogo
     * @return Products
     */
    public function setImgLogo($imgLogo)
    {
        $this->imgLogo = $imgLogo;

        return $this;
    }

    /**
     * Get imgLogo
     *
     * @return string 
     */
    public function getImgLogo()
    {
        return $this->imgLogo;
    }

    /**
     * Set imgIcono
     *
     * @param string $imgIcono
     * @return Products
     */
    public function setImgIcono($imgIcono)
    {
        $this->imgIcono = $imgIcono;

        return $this;
    }

    /**
     * Get imgIcono
     *
     * @return string 
     */
    public function getImgIcono()
    {
        return $this->imgIcono;
    }

    /**
     * Set imgFicha
     *
     * @param string $imgFicha
     * @return Products
     */
    public function setImgFicha($imgFicha)
    {
        $this->imgFicha = $imgFicha;

        return $this;
    }

    /**
     * Get imgFicha
     *
     * @return string 
     */
    public function getImgFicha()
    {
        return $this->imgFicha;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Products
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set prospecto
     *
     * @param string $prospecto
     * @return Products
     */
    public function setProspecto($prospecto)
    {
        $this->prospecto = $prospecto;

        return $this;
    }

    /**
     * Get prospecto
     *
     * @return string 
     */
    public function getProspecto()
    {
        return $this->prospecto;
    }
    
    /**
     * Set bannerProduct
     *
     * @param array BannerProduct
     * @return Products
     */
    public function setProductBanner($bannerProduct)
    {
        $this->productBanner = $bannerProduct;

        return $this;
    }

    /**
     * Get bannerProduct
     *
     * @return string 
     */
    public function getProductBanner()
    {
        return $this->productBanner;
    }

    /**
     * Add bannerProduct
     *
     * @param \Dinamic\Rovi\ProductsBundle\Entity\ProductBanner $productBanner
     * @return Products
     */
    public function addProductBanner(ProductBanner $productBanner)
    {
        $this->productBanner[] = $productBanner;

        return $this;
    }

    public function removeProductBanner(ProductBanner $productBanner)
    {
        $this->productBanner->removeElement($productBanner);
    }
}
