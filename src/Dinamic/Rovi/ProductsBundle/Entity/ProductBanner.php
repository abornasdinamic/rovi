<?php

namespace Dinamic\Rovi\ProductsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BannerProducts
 * 
 * @ORM\Table(name="rovi_product_banners")
 * @ORM\Entity(repositoryClass="Dinamic\Rovi\Rovi\ProductsBundle\Repository\ProductBannerRepository")
 */
class ProductBanner
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="bannerProducts")
     */
    private $product;
    
    /**
     * @var string
     *
     * @ORM\Column(name="imgBanner", type="string", length=75)
     */
    private $imgBanner;

    /**
     * @var string
     *
     * @ORM\Column(name="urlBanner", type="string", length=125)
     */
    private $urlBanner;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set imgBanner
     *
     * @param string $imgBanner
     * @return BannerProducts
     */
    public function setImgBanner($imgBanner)
    {
        $this->imgBanner = $imgBanner;

        return $this;
    }

    /**
     * Get imgBanner
     *
     * @return string 
     */
    public function getImgBanner()
    {
        return $this->imgBanner;
    }

    /**
     * Set urlBanner
     *
     * @param string $urlBanner
     * @return BannerProducts
     */
    public function setUrlBanner($urlBanner)
    {
        $this->urlBanner = $urlBanner;

        return $this;
    }

    /**
     * Get urlBanner
     *
     * @return string 
     */
    public function getUrlBanner()
    {
        return $this->urlBanner;
    }
    
    /**
     * Product relationship
     */
    
    /**
     * Set product
     *
     * @param int $product
     * @return BannerProducts
     */
    public function setProduct(\Dinamic\Rovi\ProductsBundle\Entity\Products $product)
    {
        $this->product = $product;
    }

    /**
     * Get product
     *
     * @return int 
     */
    public function getProduct()
    {
        return $this->product;
    }
}
